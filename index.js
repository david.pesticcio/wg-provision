const os = require("os");
const PORT = process.env.PORT || 3000;
const VPN_INTERFACE = process.env.VPN_INTERFACE || "wg0";

const autodetected = Object.entries(os.networkInterfaces())
  .map(([iface, bindings]) => [
    iface,
    bindings.filter(
      b => !b.internal && b.mac !== "00:00:00:00:00:00" && b.family === "IPv4"
    )
  ])
  .filter(a => a[1].length)[0][0];

const GATEWAY_INTERFACE =
  process.env.GATEWAY_INTERFACE || autodetected || "eth0";

const Koa = require("koa");
const bodyParser = require("koa-bodyparser");
const Views = require("koa-ejs");
const error = require("koa-error");
const logger = require("koa-logger");
const static = require("koa-static");
const { resolve } = require("path");
const Router = require("koa-router");
const session = require("koa-session");
const CSRF = require("koa-csrf");
const qrCode = require("qrcode");

const ejs = require("ejs");
const { promisify } = require("util");
const dns = require("dns");
const { fromLong, toLong, isPrivate } = require("ip");
const { parse } = require("ini");

const reverse = promisify(dns.reverse);

const renderFile = promisify(ejs.renderFile);

const uuid = require("uuid/v1");
const { readdir, readFile, writeFile, unlink } = require("fs").promises;

const wireguard = require("./wireguard");
const adminMiddleware = require("./middlewares/admin.middleware.js");

const app = new Koa();
const router = new Router();
const viewsPath = resolve(__dirname, "views");
const invitesPath = resolve(__dirname, "invites");
const claimedPath = resolve(invitesPath, "claimed");

const inviteRegexp = /^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$/i;

const interface = os.networkInterfaces()[VPN_INTERFACE][0];
const gateway = os
  .networkInterfaces()[GATEWAY_INTERFACE] // From Config
  .find(x => x.family === "IPv4" && !isPrivate(x.address));

const HOST = process.env.HOST || interface.address || "127.0.0.1";

app.keys = require("./keys.json");
app.proxy = true;

app.use(error({ engine: "ejs", template: resolve(viewsPath, "_error.ejs") }));
app.use(logger());
app.use(static(resolve(__dirname, "public")));
app.use(session(app));

app.use(bodyParser({ strict: true }));
app.use(new CSRF());
app.use((ctx, next) => {
  ctx.state.csrf = ctx.csrf;
  return next();
});

Views(app, { root: viewsPath, viewExt: "ejs", cache: false, async: true });

router.get("/", async ctx => {
  const interfaces = await wireguard.interfaces();
  await ctx.render("index", {
    interfaces
  });
});

router.post("/", async ctx => {
  const { invite_code } = ctx.request.body;

  if (!invite_code.match(inviteRegexp)) {
    throw new Error("Invalid invite code: " + invite_code);
  }

  let config;

  // Look up claimed info
  try {
    config = await readFile(resolve(claimedPath, invite_code), {
      encoding: "utf8"
    });
  } catch (ex) {
    //
  }
  const host = await reverse(gateway.address);
  const dns = process.env.DNS || interface.address;
  let invite_for, requested_by;

  if (!config) {
    try {
      const invite = await readFile(resolve(invitesPath, invite_code)).then(
        JSON.parse
      );

      invite_for = invite.invite_for;
      requested_by = invite.requested_by;
    } catch (ex) {
      throw new Error("Invalid invite code");
    }

    const privKey = await wireguard.privKey();
    const pubKey = await wireguard.pubKey(privKey);
    const {
      [VPN_INTERFACE]: { publicKey, listenPort, peers }
    } = await wireguard.data();

    const nextPeer = Object.keys(peers).length + 1;

    const address = fromLong(toLong(interface.address) + nextPeer);

    const data = {
      privKey,
      publicKey,
      listenPort,
      address,
      host,
      dns,
      invite_for,
      requested_by,
      invite_code
    };
    const template = await renderFile(resolve(__dirname, "template.ejs"), data);

    if (!process.env.BYPASS) {
      await writeFile(resolve(claimedPath, invite_code), template);
      await unlink(resolve(invitesPath, invite_code));

      await wireguard.addPeer(VPN_INTERFACE, pubKey, address);
    }

    config = template;
  }
  const configQR = await qrCode.toDataURL(config);

  await ctx.render("provision", { configQR, config, host, dns });
});

router.get("/invites", adminMiddleware(interface), async ctx => {
  const invite_code = uuid();

  const files = await readdir(invitesPath);

  const invites = await Promise.all(
    files
      .filter(invite => invite !== "claimed")
      .map(path =>
        readFile(resolve(invitesPath, path), { encoding: "ascii" })
          .then(JSON.parse)
          .then(i => ({ ...i, invite_code: path }))
      )
  );

  await ctx.render("invite", { invite_code, invites });
});

router.post("/invites", adminMiddleware(interface), async ctx => {
  const { invite_for, invite_code } = ctx.request.body;
  const requested_by = ctx.request.ip;

  await writeFile(
    resolve(invitesPath, invite_code),
    JSON.stringify({ requested_by, invite_for })
  );

  await ctx.render("granted", { invite_for, invite_code });
});

router.get("/claimed", adminMiddleware(interface), async ctx => {
  const {
    [VPN_INTERFACE]: { peers }
  } = await wireguard.data();

  const files = await readdir(claimedPath);
  const invites = await Promise.all(
    files
      .filter(path => path.match(inviteRegexp))
      .map(path =>
        readFile(resolve(claimedPath, path), { encoding: "ascii" }).then(
          file => {
            const ini = parse(file);

            const comments = file
              .split(/[\r\n]/)
              .filter(line => line.match(/^#/))
              .reduce((acc, comment) => {
                const [, key, value] = comment.match(/^# ([A-z]*) = (.*)$/);

                return { ...acc, [key]: value };
              }, {});

            return wireguard
              .pubKey(ini.Interface.PrivateKey)
              .then(pubKey => ({
                ...ini,
                comments,
                invite_code: path,
                pubKey,
                peer: peers[pubKey]
              }));
          }
        )
      )
  );

  await ctx.render("claimed", { invites });
});

app.use(router.routes()).use(router.allowedMethods());
app.listen(PORT, HOST, async function onListening() {
  const { address, port } = this.address();
  console.log(`Server listening on http://${address}:${port}/`);
  console.log(
    `Wireguard interface:\t${VPN_INTERFACE}.\tAddress: ${interface.address}`
  );
  console.log(
    `Gateway interface:\t${GATEWAY_INTERFACE}.\tAddress: ${gateway.address}`
  );
  const host = await reverse(gateway.address);
  console.log(`External hostname:\t${host}`);
});
