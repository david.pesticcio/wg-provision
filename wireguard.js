const { promisify } = require("util");
const childProcess = require("child_process");

const exec = promisify(childProcess.exec);
const execFile = promisify(childProcess.execFile);

async function data() {
  const { stdout } = await execFile("./wg-json");

  return JSON.parse(stdout);
}

module.exports = {
  data() {
    return data();
  },
  async interfaces() {
    const settings = await data();

    return Object.keys(settings);
  },
  async privKey() {
    const { stdout } = await execFile("wg", ["genkey"]);
    return stdout.split(/[\r\n]+/).filter(x => !!x).join('');
  },
  async pubKey(privKey) {
    const { stdout } = await exec(`echo "${privKey}" | wg pubkey`);

    return stdout.split(/[\r\n]+/).filter(x => !!x).join('');
  },
  async addPeer(interface, pubKey, address) {
    const { stdout } = await execFile("wg", ["set", interface, 'peer', pubKey, 'allowed-ips', `${address}/32`]);

    return stdout;
  }
};
