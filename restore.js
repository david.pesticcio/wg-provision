#!/bin/env node

const fs = require("fs").promises;
const { parse } = require("ini");
const { resolve } = require("path");

const wireguard = require("./wireguard");

const invitesPath = resolve(__dirname, "invites");
const claimedPath = resolve(invitesPath, "claimed");

async function main() {
  const invites = await fs.readdir(claimedPath);

  for (const path of invites) {
    const file = await fs.readFile(resolve(claimedPath, path), {
      encoding: "ascii"
    });

    const parsed = parse(file);

    if (!parsed.Interface) {
      continue;
    }

    const { PrivateKey, Address: FullAddress } = parsed.Interface;

    const [Address] = FullAddress.split('/');

    const pubKey = await wireguard.pubKey(PrivateKey);

    await wireguard.addPeer('wg0', pubKey, Address);
  }
}

main().catch(ex => {
  console.error(ex);
  process.exit(1);
});
