const { cidrSubnet } = require("ip");

module.exports = (interface) => (ctx, next) => {
  if(process.env.BYPASS) {
    return next();
  }
  if(cidrSubnet(interface.cidr).contains(ctx.request.ip)) {
    return next();
  }
};
